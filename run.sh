#!/usr/bin/env bash

ln -fs "/srv/conda/envs/notebook/lib/libLLVM-8.so" "libLLVM-8.so.1"

read -r -d '' program_source < "/dev/stdin"

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH::/srv/conda/envs/notebook/lib/"
export LD_PRELOAD="/home/jovyan/libLLVM-8.so.1"

# Although this outputs as LLVM assembly language,
# it can be changed to output executable code directly
./compiler <<< "$program_source" &> "out.as"
exit_code=$?

# Output is required to be on stderr in project,
# so use exit code to determine if an error occurred
if [ $exit_code -eq 1 ]; then
    cat >&2 "out.as"
    exit $exit_code
fi

# Generate LLVM bitcode
llvm-as "out.as"

# Convert LLVM bitcode into assembly
llc -O3 "out.as.bc"

# Compile assembly into executable
gcc -no-pie "out.as.s" "stdlib.o" -o "a.out"

./a.out

rm -f "a.out" "out.as" "out.as.s" "out.as.bc"
