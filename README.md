[![Live demo][live-demo-badge]][live-demo-url]


Introduction
===

This repository is part of a term project in a course
regarding the principles of compiler design.
The goal is "given a language specification, implement
a program to lex, parse, and generate LLVM assembly for
that language".

Since this project is reused by the professor year-to-year,
only the binary file is provided to discourage plagiarism.


Grammar specification ([source][grammar-source-url])
===

```
Program = Externs package identifier "{" FieldDecls MethodDecls "}" .

Externs = { ExternDefn } .
ExternDefn = extern func identifier "(" [ { ExternType }+, ] ")" MethodType ";" .

FieldDecls = { FieldDecl } .
FieldDecl = var { identifier }+, Type ";" .
FieldDecl = var { identifier }+, ArrayType ";" .
FieldDecl = var identifier Type "=" Constant ";" .

MethodDecls = { MethodDecl } .
MethodDecl = func identifier "(" [ { identifier Type }+, ] ")" MethodType Block .

Block = "{" VarDecls Statements "}" .

VarDecls = { VarDecl } .
VarDecl = var { identifier }+, Type ";" .

Statements = { Statement } .
Statement = Block .
Statement = Assign ";" .

Assign = Lvalue "=" Expr .
Lvalue = identifier | identifier "[" Expr "]" .

Statement = MethodCall ";" .

MethodCall = identifier "(" [ { MethodArg }+, ] ")" .
MethodArg = Expr | string_lit .

Statement = if "(" Expr ")" Block [ else Block ] .
Statement =  while "(" Expr ")" Block .
Statement = for "(" { Assign }+, ";" Expr ";" { Assign }+, ")" Block .
Statement = return [ "(" [ Expr ] ")" ] ";" .
Statement = break ";" .
Statement = continue ";" .

Expr = identifier .
Expr = MethodCall .
Expr = Constant .

UnaryOperator = ( UnaryNot | UnaryMinus ) .
UnaryNot = "!" .
UnaryMinus = "-" .

BinaryOperator = ( ArithmeticOperator | BooleanOperator ) .
ArithmeticOperator = ( "+" | "-" | "*" | "/" | "<<" | ">>" | "%" ) .
BooleanOperator = ( "==" | "!=" | "<" | "<=" | ">" | ">=" | "&&" | "||" ) .

Expr = Expr BinaryOperator Expr .
Expr = UnaryOperator Expr .
Expr = "(" Expr ")" .
Expr = identifier "[" Expr "]" .

ExternType = ( string | Type ) .
Type = ( int | bool ) .
MethodType = ( void | Type ) .
BoolConstant = ( true | false ) .
ArrayType = "[" int_lit "]" Type .
Constant = ( int_lit | char_lit | BoolConstant ) .
```

The list of available `extern`s is:
- `extern func print_int(int) void;`
- `extern func print_string(string) void;`
- `extern func read_int() int;`



[live-demo-badge]: <https://img.shields.io/badge/Launch-live demo (MyBinder)-blue>
[live-demo-url]: https://mybinder.org/v2/gl/JonstonChan%2Fdecaf-compiler/HEAD?urlpath=lab/tree/main.ipynb

[grammar-source-url]: https://github.com/anoopsarkar/compilers-class/blob/a4aa71d5d61419d78b56d88b0f6f3f8fb15b52d5/decafspec.md?plain=1#L573
